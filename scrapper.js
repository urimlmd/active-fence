import cheerio from 'cheerio';
import fetch from 'node-fetch';

export function fetchUrl (url) {
  return fetch(url).then(response => {
      return response.text();
  }).then(html => {
      //console.log(`fetch complete: ${html}`)
      return html;
  });
}

export function loadHtmlObject (htmlString) {
  const $ = cheerio.load(htmlString);
  return $;
}

export function scrap ($) {
  const links = $('a').get();
  const urls = links.map(a => {
      const url = a.attribs['href'];
      return url;
  });
  return urls;
}