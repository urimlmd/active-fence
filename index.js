import { fetchUrl, loadHtmlObject, scrap} from './scrapper.js';

var urls = ["https://uri-melamed.web.app/"];

urls.forEach(function (url) {
    fetchUrl(url)
        .then(loadHtmlObject)
        .then(scrap)
        .then(urls => console.log(urls))
        .catch((err) => console.warn('Something went wrong.', err));
});

